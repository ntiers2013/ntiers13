package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import demo.Customer;

/**
 * Servlet implementation class FirstServlet
 */
public class FirstServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String langue;
	private String monnaie;
       

    
	@Override
	public void init() throws ServletException {
		super.init();
		langue = getInitParameter("langue");
		monnaie = getInitParameter("monnaie");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html>");
		out.println("<head>");
		out.println("<title>");
		out.println("Test Servlet");
		out.println("</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("</body>");
		out.println("Hello ");
		try {
			Customer customer=(Customer) request.getSession().getAttribute("cust");
			
			if (customer==null){
				 customer = new Customer(1, "Dupont");
				 request.getSession().setAttribute("cust", customer); 
			}
			
			out.println(customer.getName());
		} catch (Exception e) {
			out.println("pas de customer dans le contexte de session");
		}
		out.println("<br>");
		out.println("langue="+langue);
		out.println("<br>");
		out.println("monnaie="+monnaie);
		out.println("<br>");
		out.println("date="+new Date());
		out.println("<br>");
	
		out.println("</html>");
	}


}
