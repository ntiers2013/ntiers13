package demo;
import java.util.ArrayList;
import java.util.List;

public class TestCustomer {
	
	public static final String CUSTOMER_LIST = "custs";
	public static final String CUSTOMER_CURRENT = "cust";
	
	
	public static List<Customer> mesCustomers=new ArrayList<Customer>();
	
	public static void main(String[]args){
		mesCustomers.add(new Customer(1,"Dupont","Pierre", "10 rue des lilas"));
		mesCustomers.add(new Customer(2,"Durand","Paul", "20 rue des roses"));
		mesCustomers.add(new Customer(3,"Dujnou","Jacques", "40 rue des violettes"));
		
		for (Customer customer : mesCustomers) {
			System.out.println(customer);
		}
	
	}

}
