package demo;

public class Customer {
	
	private int id;
	private String name;
	private String prenom;
	private String adresse;
	
	

	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Customer(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	
	public Customer(int id, String name, String prenom, String adresse) {
		super();
		this.id = id;
		this.name = name;
		this.prenom = prenom;
		this.adresse = adresse;
	}
	
	
	

	
	@Override
	public String toString(){
		return id+";"+name+";"+prenom+";"+adresse;
	}
	
	
}
